
## Areas y subtemas

* Conocimiento básico:
    - Tipos de datos: dict, tuple, str, int (1)
    - Ciclos (1)
    - Condiciones
    - Manejo de excepciones 
    - Manejo de librerías
    - Funciones (1)

* Conocimiento intermedio
    - Depuración de código
    - OOP
    - Uso básico de pip
    - Uso básico de virtualenv
    - Expresiones regulares
    - Uso de API's
    - Manejo de archivos
    - Lectura y escritura de json
    - Paths

* Conocimiento avanzado
    - Unit tests, mock functions
    - Type linting
    - Uso de IDE's/editores
    - Packaging apps
    - Magic methods
    - La sentencia `with` y el protocolo de manejo de contexto (context manager protocol)
    - Decoradores (de clases y de funciones) / Decoradores con argumentos
    - properties, staticmethod, classmethod
    - Mokeypatch
    - Patrones de diseño
    - Manejar múltiples versiones de python
    - Metaprogramación (getattr, setattr, bases, metaclases, reflexión, introspección)
    - Diferencias entre python2 y python3
    - Calidad de código
        + SOLID principles
        + PEP
        + Docstrings



