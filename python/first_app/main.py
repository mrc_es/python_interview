"""
Level: basic, intermediate

Interview goals:
    * Manipulating basic types
    * Date handling
    * Handling formatted files
    * Simple loops
    * Handling path names
    * Creating functions
    * Document and use typing
    * Imports
    * PEP 8 Naming convention, lower_case_with_underscores for functions
"""

import csv
import json
import pathlib

from datetime import datetime
from typing import List
from typing import Dict


def get_books(data_file: pathlib.Path) -> List[List[str]]:  # [Interview]: Use typing, and docstrings
    """Read the book's info csv and return a list of records

    :param data_file: The path of the book info file
    :return: A List of records containing the info of each book
    """
    with open(data_file, "r") as books:  # [Interview]: open in bad mode
        books_reader = csv.reader(books)
        next(books_reader)  # [Interview]: Don't skip header and produce and extra field
        books = [book_info for book_info in books_reader]  # [Interview]: switch into list comprehension

    return books


def save_json(data: Dict,
              output_file: pathlib.Path) -> None:  # [Interview]: create this function
    """Save the dict in specified file as indented json

    :param data: books in {authors, qty} format
    :param output_file: str or path for saving the file
    """
    with open(output_file, "w") as output:  # [Interview]: open in bad mode
        json.dump(data, output, indent=2)


def books_to_dict(books_list: List[List[str]]) -> Dict:
    """Convert the books list to a dict
    :param books_list:
    :return: a dict with format { qty: int, authors: { book_name: str, qty: int } }
    """
    author = 0
    book_name = 1
    qty = 2

    total = 0
    books = {"authors": {}, "total": 0}

    for book in books_list:
        qty_book = int(book[qty])  # [Interview]: cast to int
        total = total + qty_book  # [Interview]: TypeError: add str to int for not casting

        record = {"book_name": book[book_name], "qty": qty_book}
        books["authors"].setdefault(book[author], []).append(record)

    books["total"] = total

    return books


def cleaned_author_name(books_to_clean: Dict) -> Dict:
    """Clean undesirable characters in the author names

    :param books_to_clean: books to clean dictionary
    :return: the dictionary cleaned in the author's fields
    """
    authors = books_to_clean.get("authors")
    for author, record in list(authors.items()):
        # [Interview]: Check if character is in string
        if author.startswith(":"):  # Or use: if ":" in author:
            cleaned_name = author.replace(":", "")  # [Interview]: Clean author name
            authors[cleaned_name] = authors.pop(author)

    return books_to_clean


def enrich_books_data(books_data: Dict) -> Dict:  # [Interview]: Create this function
    today: str = datetime.strftime(datetime.today(), "%Y-%m-%d")  # [Interview]: Date formats
    books_data["creation_date"] = today
    return books_data


def run():
    # [Interview]: the use strings vs pathlib library
    input_file = pathlib.Path("data", "books_info.csv")  # ./data/books_info.csv
    output_file = pathlib.Path("result", "books_info.json")  # ./result/books_info.json

    book_list: List[List[str]] = get_books(input_file)
    books_data: Dict = books_to_dict(book_list)
    cleaned_books_data: Dict = cleaned_author_name(books_data)
    enriched_books_data: Dict = enrich_books_data(cleaned_books_data)

    """Since dicts are mutable, the past code is the same as simply:

    books_data: Dict = books_to_dict(book_list)
    cleaned_author_name(books_data)
    enrich_books_data(cleaned_books_data)

    i.e. without assignation

    """
    save_json(enriched_books_data, output_file)


if __name__ == "__main__":
    run()
