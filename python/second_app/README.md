# App For Testing 2

## Description

The main goal of this app is to look at the knowledge and the way of using some python features
that the candidate has.

By looking into this app, we expect to see the level of knowledge of the candidate about:

* Regex
* Make http calls and knows about the http methods and headers
* Classmethods
* Property decorator
* Use of sets
* Monkey patching
* String formating
* The basic use of pip

The functions to see are `app.py` and `cleaning/cleaner.py`

The *app.py* functions work by making HTTP calls to a minimal Flask web CRUD API running in background in a parallel thread.

This program reads the output of the HTTP API and sends a response to the same API.

That API will store the name of some authors in the file `my_api/data/authors.json`

All the topics proposed for the interview are labeled with comments with `# [Interview]`

## Installation

This repo has been tested in Python 3.7.9

Run _virtualenv_ for installing the dependencies. 

The requirements are inside the _requirements.txt_ file. Use *pip* for the installation.

## Running the code

For running the code, we can simply type:

    $ python app.py

The mock server and the mock API will run by default in _127.0.0.1:1234_

We can modify this behaviour in the config namedtuple within the *app.py* file.

When finishing the program, both threads will terminate. 
