import hashlib
import sqlite3
from pathlib import Path
from typing import Set

from flask import jsonify, request
from functools import wraps


def token_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        api_key = request.headers.get("X-Api-Key", "")

        if not api_key or not token_is_valid(api_key.encode("utf-8")):
            return jsonify({"status": "error", "reason": "Authentication failed"}), 401

        return func(*args, **kwargs)

    return wrapper


def get_encoded_messages_active() -> Set[str]:
    db_path = Path("my_api", "data", "db.sqlite")
    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()
    message_active = 0
    active_encoded_messages = {result[message_active]
                               for result in cursor.execute("select token from tokens;").fetchall()}
    connection.close()
    return active_encoded_messages


def token_is_valid(api_key) -> bool:
    active_encoded_messages = get_encoded_messages_active()
    encoded = hashlib.sha512(api_key).hexdigest()

    return encoded in active_encoded_messages
