from collections import namedtuple
from multiprocessing import Process
from utils import api_utils
from my_api.my_api import flask_thread


def run_apps(my_app, config: namedtuple):
    server_thread = Process(target=flask_thread, args=(config,))
    my_app_thread = Process(target=my_app)

    server_thread.start()

    api_utils.check_until_alive(config)
    my_app_thread.start()

    my_app_thread.join()
    server_thread.terminate()
