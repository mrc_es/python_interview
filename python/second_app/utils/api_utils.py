import requests
from collections import namedtuple


def check_until_alive(config: namedtuple) -> None:

    is_alive_url: str = f"http://{config.host_name}:{config.port}/is_alive"
    while True:
        try:
            request = requests.get(is_alive_url)
            if request.status_code == 200:
                return
        except:
            pass
