import re

from typing import Set


class Cleaner:

    def __init__(self, str_to_clean):
        self.str_to_clean: str = str_to_clean

    @property  # [Interview]: Use of properties for setter and getter
    def str_to_clean(self):
        return self._str_to_clean

    @str_to_clean.setter
    def str_to_clean(self, str_to_clean):
        if not str_to_clean:
            raise ValueError("The string to clean must be not null")
        self._str_to_clean = str_to_clean

    def remove_annoying_spaces(self):
        self.str_to_clean = " ".join(self.str_to_clean.split())
        return self

    def bad_is_alpha(self):
        self.str_to_clean = re.sub(r'[^A-Za-z]', '', self.str_to_clean)  # [Interview]: regex
        return self

    def clean(self) -> str:
        return self.bad_is_alpha()\
            .remove_annoying_spaces()\
            .str_to_clean

    @classmethod
    def clean_all_authors(cls, authors_to_clean: Set) -> Set:  # [Interview]: Use of classmethod
        authors_cleaned = set()
        for author in authors_to_clean:
            cleaner = cls(author)
            authors_cleaned.add(cleaner.clean())
        return authors_cleaned
