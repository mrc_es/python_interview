import csv
import json

from collections import namedtuple
from typing import List
from flask import Flask
from flask import jsonify
from flask import request
from pathlib import Path
from utils.auth_utils import token_auth

app = Flask(__name__)


@app.route("/is_alive", methods=["GET"])
def is_alive():
    return jsonify({"status": "success"})


@app.route("/v2/save_authors", methods=["PUT"])
@token_auth
def save_authors():
    authors: List = request.get_json()
    output_file = Path("my_api", "data", "authors.json")

    with open(output_file, "w") as authors_file:
        json.dump(authors, authors_file)

    return jsonify({"status": "success"})


@app.route("/v2/all_books", methods=["GET"])
@token_auth
def all_books():
    input_file = Path("my_api", "data", "books_info.csv")
    with open(input_file, "r") as books_file:
        books_reader = csv.reader(books_file)
        next(books_reader)
        return jsonify(list(books_reader))


def flask_thread(config: namedtuple):
    app.run(host=config.host_name, port=config.port, debug=True, use_reloader=False)
