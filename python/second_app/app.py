
import requests

from cleaning.cleaner import Cleaner
from collections import namedtuple
from typing import Dict, List, Set
from utils.threading_utils import run_apps

# [START] Configuration
Config = namedtuple("Config", field_names=["host_name", "port"])
config = Config("127.0.0.1", 1234)
api_key = "My_5uPer_PA4.55Sword"
# [END] Configuration


def good_is_alpha(self):
    import re
    self.str_to_clean = re.sub(r'[^A-Za-z ]', '', self.str_to_clean)
    return self


def my_app():

    all_books_url: str = f"http://{config.host_name}:{config.port}/v2/all_books"  # [Interview]: F'strings
    put_authors_url: str = f"http://{config.host_name}:{config.port}/v2/save_authors"

    all_books_request = requests.get(all_books_url, headers={"X-Api-Key": api_key})  # [Interview]: make http request with headers

    books: Dict = all_books_request.json()

    author = 0
    authors: Set = {book[author] for book in books}  # [Interview]: Use of Set

    Cleaner.bad_is_alpha = good_is_alpha  # [Interview]: Use of monkey patch
    authors_cleaned: List = list(Cleaner.clean_all_authors(authors))  # [Interview]: classmethod

    requests.put(put_authors_url, json=authors_cleaned, headers={"X-Api-Key": api_key})  # [Interview]: make http request with headers


if __name__ == "__main__":
    run_apps(my_app, config)
